@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                  <center><h1 style="color:#1E7FCB"><b>{{ trans('etudiant.detailetudiant')}}</b></h1></center>
                </div>
                <center>
      <div class="panel-body">
        @if (session('status'))
          <div class="alert alert-success">
            {{ session('status') }}
          </div>
        @endif


        <p class="lead"> <u><b>ID</b></u> :  {{ $etudiant->id }}  </p>
        <p class="lead"> <u><b>Nom</b></u> :  {{ $etudiant->nom }} </p>
        <p class="lead"> <u><b>Prenom</b></u> : {{ $etudiant->prenom }}</p>
        <hr>

          <a href="{{ route('home')}} " class="btn btn-info">Back</a>

      </div>
    </center>
    </div>
  </div>
</div>
@endsection
