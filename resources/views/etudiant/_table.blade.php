<table class="table">
  <tr>
    <th>
      {{ trans('etudiant.nom') }}
    </th>
    <th>
      {{ trans('etudiant.prenom') }}
    </th>
    <th>
      Action
    </th>
    <th></th>
    <th></th>

  </tr>
  @foreach ($etudiants as $etudiant)
  <tr>
    <th>
      {{ $etudiant->nom }}
    </th>
    <th>
      {{ $etudiant->prenom }}
    </th>
    <th>
      <a href="{{ route('showEtudiant', $etudiant->id) }}" class="btn btn-primary"> {{ trans('commun.details') }}</a>
    </th>
    <th>
      <a href="{{ route('editEtudiant', $etudiant->id) }}" class="btn btn-success"> {{ trans('commun.modifier') }} </a>
    </th>
    <th>
      <form action="{{action('EtudiantController@destroy', $etudiant->id)}}" method="post">
                    {{csrf_field()}}
                    <input name="_method" type="hidden" value="DELETE">
                    <button class="btn btn-danger" type="submit"> {{ trans('commun.supprimer') }}</button>
      </form>
    </th>
  </tr>
  @endforeach
</table>
