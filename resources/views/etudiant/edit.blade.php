@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                  <center><h1 style="color:#1E7FCB"><b>{{ trans('etudiant.titremodification')}}</b></h1></center>
                </div>
                <br>
                <center>
      <div class="panel-body">
        @if (session('status'))
          <div class="alert alert-success">
            {{ session('status') }}
          </div>
        @endif
        @include('etudiant._form')
      </div>
    </div>
  </center>
    </div>
  </div>
</div>
@endsection
