<?php

return [
  'nom' => 'Nom',
  'prenom' => 'Prenom',
  'titremodification' => 'Modification Etudiant',
  'detailetudiant' => 'Details Etudiant',
  'msgsuppressionok' => 'Suppression effectue avec succes',
  'msgenregistrementok' => 'Enregistrement effectue avec succes',
  'msgmiseajourok' => 'Mise a jour effectue avec succes'
];
