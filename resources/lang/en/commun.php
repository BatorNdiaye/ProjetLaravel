<?php

return [
  'enregistrer' => 'Save',
  'details' => 'Detail',
  'modifier' => 'Update',
  'supprimer' => 'Delete'
];
